﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RO;

namespace ROOOP
{
    public partial class NewRole : Form
    {
        public NewRole()
        {
            InitializeComponent();
        }

        private void btn_AddRole_Click(object sender, EventArgs e)
        {

        }

        private void lb_STR_Click(object sender, EventArgs e)
        {


        }




        #region 增加能力素質

        short pSTR = 5; short pINT = 5;    //STR對應INT
        short pAGI = 5; short pLUK = 5;    //AGI對應LUK
        short pDEX = 5; short pVIT = 5;    //DEX對應VIT
        private void AddAbility(EAbility abty)
        {
            switch (abty)
            {
                case EAbility.STR: if (pINT > 1) { pSTR += 1; pINT -= 1; } break;
                case EAbility.AGI: if (pLUK > 1) { pAGI += 1; pLUK -= 1; } break;
                case EAbility.VIT: if (pDEX > 1) { pVIT += 1; pDEX -= 1; } break;
                case EAbility.INT: if (pSTR > 1) { pINT += 1; pSTR -= 1; } break;
                case EAbility.DEX: if (pVIT > 1) { pDEX += 1; pVIT -= 1; } break;
                case EAbility.LUK: if (pAGI > 1) { pLUK += 1; pAGI -= 1; } break;
                default: break;
            }

            lb_STR.Text = pSTR.ToString();
            lb_AGI.Text = pAGI.ToString();
            lb_VIT.Text = pVIT.ToString();
            lb_INT.Text = pINT.ToString();
            lb_DEX.Text = pDEX.ToString();
            lb_LUK.Text = pLUK.ToString();
        }

        private void btn_STR_Click(object sender, EventArgs e) { AddAbility(EAbility.STR); }
        private void btn_VIT_Click(object sender, EventArgs e) { AddAbility(EAbility.VIT); }
        private void btn_LUK_Click(object sender, EventArgs e) { AddAbility(EAbility.LUK); }
        private void btn_INT_Click(object sender, EventArgs e) { AddAbility(EAbility.INT); }
        private void btn_DEX_Click(object sender, EventArgs e) { AddAbility(EAbility.DEX); }
        private void btn_AGI_Click(object sender, EventArgs e) { AddAbility(EAbility.AGI); }

        #endregion


        #region 變更角色性別

        private void rb_girl_CheckedChanged(object sender, EventArgs e)
        {
            changeSexImage(ESex.girl);
        }

        private void rb_boy_CheckedChanged(object sender, EventArgs e)
        {
            changeSexImage(ESex.boy);
        }

        private void changeSexImage(ESex sex)
        {
            switch (sex)
            {
                case ESex.girl: pb_sex.Image = Properties.Resources._01_f; break;
                case ESex.boy : pb_sex.Image = Properties.Resources._01_m; break;
                default: break;
            }
        }

        #endregion



    }
}
