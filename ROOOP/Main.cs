﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RO;


namespace ROOOP
{
    public partial class FrmMain : Form
    {

        #region Form事件處理

        public FrmMain() {
            InitializeComponent();
        }


        private void FrmMain_Load(object sender, EventArgs e)
        {
        }

        private void FrmMain_Closing(object sender, CancelEventArgs e)
        {
            //詢問是否要關閉主視窗，否則取消FrmMain_Closing事件，要關閉則進入FrmMain_Closed事件。
            DialogResult dialogResult = MessageBox.Show("關閉ROOOP?", "確定要離開了?", MessageBoxButtons.YesNo);
            if (dialogResult != DialogResult.Yes)
                e.Cancel = true;
        }

        private void FrmMain_Closed(object sender, FormClosedEventArgs e)
        {
            //FrmMain_Closing事件若取消，則不會進到FrmMain_Closed，反之則會進入FrmMain_Closed事件，進入後則結束整個程式。
            Application.Exit();
        }

        #endregion


        #region 計時器

        System.Timers.Timer TMonsterReborn = new System.Timers.Timer();

        void TMonsterReborn_Set()
        {
            TMonsterReborn.Interval = 1000;
            TMonsterReborn.Elapsed  += TMonsterReborn_Tick;
            TMonsterReborn.Enabled = true;
        }

        void TMonsterReborn_Tick(Object source, System.Timers.ElapsedEventArgs e)
        {

        }

        #endregion


        #region 新增角色
        Role MyRole;
        private void button1_Click(object sender, EventArgs e)
        {
            MyRole = new Novice("羅羅亞·索隆", ESex.boy);   //新增角色物件；用建構子新增角色名稱&性別
            getRoleInfo();
        }

        private void getRoleInfo() {

            gb_Name.Text = MyRole.Name;

            lb_LV.Text = MyRole.Level.ToString();
            lb_HP.Text = MyRole.getNowHP().ToString() + "/" + MyRole.getTotalHP().ToString();
            lb_SP.Text = MyRole.getNowSP().ToString() + "/" + MyRole.getTotalSP().ToString();
            lb_EXP.Text = "123/456";

            pb_HP.Maximum = MyRole.getTotalHP();
            pb_SP.Maximum = MyRole.getTotalSP();
            pb_EXP.Maximum = 100;

            pb_HP.Value = pb_HP.Maximum;
            pb_SP.Value = pb_SP.Maximum;
            pb_EXP.Value = 10;

            lb_ATK.Text = MyRole.getATK().ToString();
            lb_STR.Text = MyRole.STR.ToString();
            lb_VIT.Text = MyRole.VIT.ToString();
            lb_INT.Text = MyRole.INT.ToString();
        }

        #endregion



        #region 功能按鈕


        private void AddAbility(string aType)
        {
            short point = short.Parse(lb_Point.Text);
            if (point > 0)
            {
                switch (aType)
                {
                    case "STR": MyRole.addSTR(); lb_STR.Text = MyRole.STR.ToString(); break;
                    //case "AGI": MyRole.addAGI(); lb_AGI.Text = MyRole.AGI.ToString(); break;
                    case "VIT": MyRole.addVIT(); lb_VIT.Text = MyRole.VIT.ToString(); break;
                    case "INT": MyRole.addINT(); lb_INT.Text = MyRole.INT.ToString(); break;
                    //case "DEX": MyRole.addDEX(); lb_DEX.Text = MyRole.DEX.ToString(); break;
                    //case "LUK": MyRole.addLUK(); lb_LUK.Text = MyRole.LUK.ToString(); break;
                    default: break;
                }     
                point -= 1;
                lb_Point.Text = point.ToString();
            }
        }

        private void ll_STR_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AddAbility("STR");
            //short point = short.Parse(lb_Point.Text);
            //if (point > 0)
            //{
            //    MyRole.addSTR();
            //    lb_STR.Text = MyRole.STR.ToString();
            //    point -= 1;
            //    lb_Point.Text = point.ToString();

            //}
        }

        private void ll_VIT_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AddAbility("VIT");
            //short point = short.Parse(lb_Point.Text);
            //if (point > 0)
            //{
            //    MyRole.addVIT();
            //    lb_VIT.Text = MyRole.VIT.ToString();
            //    point -= 1;
            //    lb_Point.Text = point.ToString();

            //}
        }

        private void ll_INT_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AddAbility("INT");
            //short point = short.Parse(lb_Point.Text);
            //if (point > 0)
            //{
            //    MyRole.addINT();
            //    lb_INT.Text = MyRole.INT.ToString();
            //    point -= 1;
            //    lb_Point.Text = point.ToString();

            //}
        }




        #endregion

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
