﻿namespace ROOOP
{
    partial class NewRole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewRole));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pb_sex = new System.Windows.Forms.PictureBox();
            this.rb_boy = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_Name = new System.Windows.Forms.TextBox();
            this.btn_AGI = new System.Windows.Forms.Button();
            this.btn_DEX = new System.Windows.Forms.Button();
            this.btn_INT = new System.Windows.Forms.Button();
            this.btn_LUK = new System.Windows.Forms.Button();
            this.btn_VIT = new System.Windows.Forms.Button();
            this.btn_STR = new System.Windows.Forms.Button();
            this.lb_LUK = new System.Windows.Forms.Label();
            this.lb_DEX = new System.Windows.Forms.Label();
            this.lb_INT = new System.Windows.Forms.Label();
            this.lb_VIT = new System.Windows.Forms.Label();
            this.lb_AGI = new System.Windows.Forms.Label();
            this.lb_STR = new System.Windows.Forms.Label();
            this.btn_AddRole = new System.Windows.Forms.Button();
            this.rb_girl = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_sex)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(580, 347);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pb_sex
            // 
            this.pb_sex.Image = ((System.Drawing.Image)(resources.GetObject("pb_sex.Image")));
            this.pb_sex.Location = new System.Drawing.Point(67, 139);
            this.pb_sex.Margin = new System.Windows.Forms.Padding(2);
            this.pb_sex.Name = "pb_sex";
            this.pb_sex.Size = new System.Drawing.Size(100, 100);
            this.pb_sex.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pb_sex.TabIndex = 3;
            this.pb_sex.TabStop = false;
            // 
            // rb_boy
            // 
            this.rb_boy.AutoSize = true;
            this.rb_boy.BackColor = System.Drawing.Color.White;
            this.rb_boy.Checked = true;
            this.rb_boy.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rb_boy.Location = new System.Drawing.Point(67, 114);
            this.rb_boy.Name = "rb_boy";
            this.rb_boy.Size = new System.Drawing.Size(38, 20);
            this.rb_boy.TabIndex = 4;
            this.rb_boy.TabStop = true;
            this.rb_boy.Text = "男";
            this.rb_boy.UseVisualStyleBackColor = false;
            this.rb_boy.CheckedChanged += new System.EventHandler(this.rb_boy_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.txt_Name);
            this.panel1.Controls.Add(this.btn_AGI);
            this.panel1.Controls.Add(this.btn_DEX);
            this.panel1.Controls.Add(this.btn_INT);
            this.panel1.Controls.Add(this.btn_LUK);
            this.panel1.Controls.Add(this.btn_VIT);
            this.panel1.Controls.Add(this.btn_STR);
            this.panel1.Controls.Add(this.lb_LUK);
            this.panel1.Controls.Add(this.lb_DEX);
            this.panel1.Controls.Add(this.lb_INT);
            this.panel1.Controls.Add(this.lb_VIT);
            this.panel1.Controls.Add(this.lb_AGI);
            this.panel1.Controls.Add(this.lb_STR);
            this.panel1.Controls.Add(this.btn_AddRole);
            this.panel1.Controls.Add(this.rb_girl);
            this.panel1.Controls.Add(this.rb_boy);
            this.panel1.Controls.Add(this.pb_sex);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(585, 354);
            this.panel1.TabIndex = 5;
            // 
            // txt_Name
            // 
            this.txt_Name.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Name.Location = new System.Drawing.Point(67, 248);
            this.txt_Name.Name = "txt_Name";
            this.txt_Name.Size = new System.Drawing.Size(105, 22);
            this.txt_Name.TabIndex = 19;
            this.txt_Name.Text = "ROOOP";
            // 
            // btn_AGI
            // 
            this.btn_AGI.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btn_AGI.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btn_AGI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AGI.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AGI.Location = new System.Drawing.Point(195, 114);
            this.btn_AGI.Name = "btn_AGI";
            this.btn_AGI.Size = new System.Drawing.Size(42, 31);
            this.btn_AGI.TabIndex = 18;
            this.btn_AGI.Text = "AGI";
            this.btn_AGI.UseVisualStyleBackColor = false;
            this.btn_AGI.Click += new System.EventHandler(this.btn_AGI_Click);
            // 
            // btn_DEX
            // 
            this.btn_DEX.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btn_DEX.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btn_DEX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_DEX.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DEX.Location = new System.Drawing.Point(195, 198);
            this.btn_DEX.Name = "btn_DEX";
            this.btn_DEX.Size = new System.Drawing.Size(42, 31);
            this.btn_DEX.TabIndex = 17;
            this.btn_DEX.Text = "DEX";
            this.btn_DEX.UseVisualStyleBackColor = false;
            this.btn_DEX.Click += new System.EventHandler(this.btn_DEX_Click);
            // 
            // btn_INT
            // 
            this.btn_INT.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btn_INT.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btn_INT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_INT.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_INT.Location = new System.Drawing.Point(273, 252);
            this.btn_INT.Name = "btn_INT";
            this.btn_INT.Size = new System.Drawing.Size(42, 31);
            this.btn_INT.TabIndex = 16;
            this.btn_INT.Text = "INT";
            this.btn_INT.UseVisualStyleBackColor = false;
            this.btn_INT.Click += new System.EventHandler(this.btn_INT_Click);
            // 
            // btn_LUK
            // 
            this.btn_LUK.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btn_LUK.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btn_LUK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_LUK.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_LUK.Location = new System.Drawing.Point(351, 199);
            this.btn_LUK.Name = "btn_LUK";
            this.btn_LUK.Size = new System.Drawing.Size(42, 31);
            this.btn_LUK.TabIndex = 15;
            this.btn_LUK.Text = "LUK";
            this.btn_LUK.UseVisualStyleBackColor = false;
            this.btn_LUK.Click += new System.EventHandler(this.btn_LUK_Click);
            // 
            // btn_VIT
            // 
            this.btn_VIT.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btn_VIT.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btn_VIT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_VIT.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_VIT.Location = new System.Drawing.Point(351, 114);
            this.btn_VIT.Name = "btn_VIT";
            this.btn_VIT.Size = new System.Drawing.Size(42, 31);
            this.btn_VIT.TabIndex = 14;
            this.btn_VIT.Text = "VIT";
            this.btn_VIT.UseVisualStyleBackColor = false;
            this.btn_VIT.Click += new System.EventHandler(this.btn_VIT_Click);
            // 
            // btn_STR
            // 
            this.btn_STR.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.btn_STR.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btn_STR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_STR.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_STR.Location = new System.Drawing.Point(273, 59);
            this.btn_STR.Name = "btn_STR";
            this.btn_STR.Size = new System.Drawing.Size(42, 31);
            this.btn_STR.TabIndex = 13;
            this.btn_STR.Text = "STR";
            this.btn_STR.UseVisualStyleBackColor = false;
            this.btn_STR.Click += new System.EventHandler(this.btn_STR_Click);
            // 
            // lb_LUK
            // 
            this.lb_LUK.AutoSize = true;
            this.lb_LUK.BackColor = System.Drawing.SystemColors.MenuBar;
            this.lb_LUK.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_LUK.Location = new System.Drawing.Point(497, 125);
            this.lb_LUK.Name = "lb_LUK";
            this.lb_LUK.Size = new System.Drawing.Size(14, 14);
            this.lb_LUK.TabIndex = 12;
            this.lb_LUK.Text = "5";
            // 
            // lb_DEX
            // 
            this.lb_DEX.AutoSize = true;
            this.lb_DEX.BackColor = System.Drawing.SystemColors.MenuBar;
            this.lb_DEX.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_DEX.Location = new System.Drawing.Point(497, 109);
            this.lb_DEX.Name = "lb_DEX";
            this.lb_DEX.Size = new System.Drawing.Size(14, 14);
            this.lb_DEX.TabIndex = 11;
            this.lb_DEX.Text = "5";
            // 
            // lb_INT
            // 
            this.lb_INT.AutoSize = true;
            this.lb_INT.BackColor = System.Drawing.SystemColors.MenuBar;
            this.lb_INT.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_INT.Location = new System.Drawing.Point(497, 93);
            this.lb_INT.Name = "lb_INT";
            this.lb_INT.Size = new System.Drawing.Size(14, 14);
            this.lb_INT.TabIndex = 10;
            this.lb_INT.Text = "5";
            // 
            // lb_VIT
            // 
            this.lb_VIT.AutoSize = true;
            this.lb_VIT.BackColor = System.Drawing.SystemColors.MenuBar;
            this.lb_VIT.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_VIT.Location = new System.Drawing.Point(497, 77);
            this.lb_VIT.Name = "lb_VIT";
            this.lb_VIT.Size = new System.Drawing.Size(14, 14);
            this.lb_VIT.TabIndex = 9;
            this.lb_VIT.Text = "5";
            // 
            // lb_AGI
            // 
            this.lb_AGI.AutoSize = true;
            this.lb_AGI.BackColor = System.Drawing.SystemColors.MenuBar;
            this.lb_AGI.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_AGI.Location = new System.Drawing.Point(497, 61);
            this.lb_AGI.Name = "lb_AGI";
            this.lb_AGI.Size = new System.Drawing.Size(14, 14);
            this.lb_AGI.TabIndex = 8;
            this.lb_AGI.Text = "5";
            // 
            // lb_STR
            // 
            this.lb_STR.AutoSize = true;
            this.lb_STR.BackColor = System.Drawing.SystemColors.MenuBar;
            this.lb_STR.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_STR.Location = new System.Drawing.Point(497, 45);
            this.lb_STR.Name = "lb_STR";
            this.lb_STR.Size = new System.Drawing.Size(14, 14);
            this.lb_STR.TabIndex = 7;
            this.lb_STR.Text = "5";
            this.lb_STR.Click += new System.EventHandler(this.lb_STR_Click);
            // 
            // btn_AddRole
            // 
            this.btn_AddRole.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_AddRole.Location = new System.Drawing.Point(487, 322);
            this.btn_AddRole.Name = "btn_AddRole";
            this.btn_AddRole.Size = new System.Drawing.Size(89, 23);
            this.btn_AddRole.TabIndex = 6;
            this.btn_AddRole.Text = "新增角色";
            this.btn_AddRole.UseVisualStyleBackColor = true;
            this.btn_AddRole.Click += new System.EventHandler(this.btn_AddRole_Click);
            // 
            // rb_girl
            // 
            this.rb_girl.AutoSize = true;
            this.rb_girl.BackColor = System.Drawing.Color.White;
            this.rb_girl.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rb_girl.Location = new System.Drawing.Point(129, 114);
            this.rb_girl.Name = "rb_girl";
            this.rb_girl.Size = new System.Drawing.Size(38, 20);
            this.rb_girl.TabIndex = 5;
            this.rb_girl.Text = "女";
            this.rb_girl.UseVisualStyleBackColor = false;
            this.rb_girl.CheckedChanged += new System.EventHandler(this.rb_girl_CheckedChanged);
            // 
            // NewRole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 354);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "NewRole";
            this.Text = "NewRole";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_sex)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pb_sex;
        private System.Windows.Forms.RadioButton rb_boy;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rb_girl;
        private System.Windows.Forms.Button btn_AddRole;
        private System.Windows.Forms.Label lb_STR;
        private System.Windows.Forms.Label lb_LUK;
        private System.Windows.Forms.Label lb_DEX;
        private System.Windows.Forms.Label lb_INT;
        private System.Windows.Forms.Label lb_VIT;
        private System.Windows.Forms.Label lb_AGI;
        private System.Windows.Forms.Button btn_STR;
        private System.Windows.Forms.Button btn_AGI;
        private System.Windows.Forms.Button btn_DEX;
        private System.Windows.Forms.Button btn_INT;
        private System.Windows.Forms.Button btn_LUK;
        private System.Windows.Forms.Button btn_VIT;
        private System.Windows.Forms.TextBox txt_Name;
    }
}