﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ROOOP
{
    public static class Program
    {
        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FrmControl();
            //Application.Run(new Login());
            Application.Run();
        }

        public static Login fLogin;
        public static FrmMain fMain;
        public static NewRole fNewRole;
        static void FrmControl()
        {
            fMain = new FrmMain();
            fLogin = new Login();
            fNewRole = new NewRole();
            fLogin.Show();
        }

    }
}
