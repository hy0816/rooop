﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RO
{


    #region 物件導向名詞解釋及中英文對照
    
    /*
     * 
     * 
     * 
     * 
     * 
     * 
     */

    #endregion


    #region 角色設定

    #region 人物設定

    public abstract class Role
    {

        public string Name { get; private set; }
        public ESex Sex { get; private set; }
        public EStep Step { get; private set; }
        public EJobType Job { get; private set; }
        public short EXP { get; private set; }
        public byte Level { get; private set; }
        
        public byte STR { get; private set; }
        public byte AGI { get; private set; }
        public byte VIT { get; private set; }
        public byte INT { get; private set; }
        public byte DEX { get; private set; }
        public byte LUK { get; private set; }

        private short HP { get; set; }
        private short SP { get; set; }

        //新增角色-建構子
        public Role(string _name, ESex _sex)
        {
            Name = _name;
            Sex = _sex;
            Step = EStep.Step0;
            Job = EJobType.初心者;
            EXP = 0;
            Level = 1;
            STR = 1;
            AGI = 1;
            VIT = 1;
            INT = 1;
            DEX = 1;
            LUK = 1;
            //HP = 100;
            //SP = 100;
        }


        //abstract class Role 抽象類別這裡的方法我宣告virtual
        //因為不一定會覆寫其方法，可以直接繼承和使用

        //設定幾轉
        public virtual void stepUP() { Step += 1; }

        //職業變更
        //public virtual void TranJobType(EJobType _job) { Job = _job; }

        //增加經驗值
        public virtual void addEXP(short e) { EXP += e; }

        //等級提升
        public virtual void levelUP() { Level += 1; }

        //取得ATK攻擊力
        //因為每個階級攻擊力不同，因此宣告為abstract，並要求不同階級時，要覆寫&實作攻擊力方法。
        public abstract short getATK();

        //增加STR、AGI、VIT、INT、DEX、LUK
        public virtual void addSTR() { STR += 1; }
        public virtual void addAGI() { AGI += 1; }
        public virtual void addVIT() { VIT += 1; }
        public virtual void addINT() { INT += 1; }
        public virtual void addDEX() { DEX += 1; }
        public virtual void addLUK() { LUK += 1; }

        //+-血量,+-魔量
        public virtual void setHP(short _hp) { HP += _hp; }
        public virtual void setSP(short _sp) { SP += _sp; }

        //取得總血量,取得總魔量
        //因為每個階級血量級距不同，因此宣告為abstract，並要求不同階級時，要覆寫&實作總血量&總魔量方法。
        public abstract short getTotalHP();
        public abstract short getTotalSP();

        //取得當前血量,取得當前魔量
        public virtual short getNowHP() { return HP; }
        public virtual short getNowSP() { return SP; }

    }

    //性別
    public enum ESex { girl = 0, boy = 1 }

    //素質
    public enum EAbility
    {
        STR = 0,
        AGI = 1,
        VIT = 2,
        INT = 3,
        DEX = 4,
        LUK = 5
    }

    //幾轉
    public enum EStep
    {
        Step0 = 0,
        Step1 = 1,
        Step2 = 2,
        Step3 = 3
    }

    //職業類別
    public enum EJobType
    {
        初心者 = 0,
        wizard = 1
    }

    #endregion



    #region 職業設定


    //初心者，繼承Role類別，並使用介面ISkillNovice
    public class Novice : Role, ISkillNovice
    {
        //繼承abstract class Role的建構子
        public Novice(string _name, ESex _sex) : base (_name, _sex) { }

        //覆寫&實作繼承自abstract class Role的方法
        public override short getATK() { short _atk = (short)(STR * 5); return _atk; }
        public override short getTotalHP() { short hp = (short)(VIT * Level * 100 * 1); return hp; }
        public override short getTotalSP() { short sp = (short)(INT * Level * 50 * 1); return sp; }

        public int 普通攻擊() {
            Random r = new Random();
            int rATK = r.Next(5);
            return getATK() * rATK;
        }
    }

    //劍士，繼承Novice類別，並使用介面ISkillSwordsman
    public class Swordsman : Novice, ISkillSwordsman
    {
        //繼承Novice類別的建構子
        public Swordsman(string _name, ESex _sex) : base(_name, _sex) { }

        //覆寫&實作繼承自abstract class Novice的方法
        public override short getATK() { short _atk = (short)(STR * 10); return _atk; }
        public override short getTotalHP() { short hp = (short)(VIT * Level * 100 * 5); return hp; }
        public override short getTotalSP() { short sp = (short)(INT * Level * 50 * 3); return sp; }

        public int 狂擊() {
            Random r = new Random();
            int rATK = r.Next(5);
            return getATK() * rATK;
        }
        public void 快速回復() { }
    }

    //騎士，繼承Swordsman類別，並使用介面ISkillKnight
    public class Knight : Swordsman, ISkillKnight
    {
        //繼承Swordsman類別的建構子
        public Knight(string _name, ESex _sex) : base(_name, _sex) { }

        //覆寫&實作繼承自abstract class Swordsman的方法
        public override short getATK() { short _atk = (short)(STR * 15); return _atk; }
        public override short getTotalHP() { short hp = (short)(VIT * Level * 100 * 10); return hp; }
        public override short getTotalSP() { short sp = (short)(INT * Level * 50 * 6); return sp; }
        
        public int 怪物互擊() {
            Random r = new Random();
            int rATK = r.Next(50);
            return getATK() * rATK;
        }
        public void 靈氣劍() { }
    }

    //騎士領主，繼承Knight類別，並使用介面ISkillLordKnight
    public class LordKnight : Knight, ISkillLordKnight
    {
        //繼承Knight類別的建構子
        public LordKnight(string _name, ESex _sex) : base(_name, _sex) { }

        //覆寫&實作繼承自abstract class Knight的方法
        public override short getATK() { short _atk = (short)(STR * 20); return _atk; }
        public override short getTotalHP() { short hp = (short)(VIT * Level * 100 * 20); return hp; }
        public short getTotalHPburn() { short hp = (short)(VIT * Level * 100 * 20 * 3); return hp; }
        public override short getTotalSP() { short sp = (short)(INT * Level * 50 * 10); return sp; }

        public int 集中攻擊() {
            Random r = new Random();
            int rATK = r.Next(100);
            return getATK() * rATK;
        }
        public void 生命點燃() {
            //設Timer30秒，30秒內，HP提升3倍
            int _hp = getNowHP();
            int x2 = 2;
            setHP((short) (_hp * x2));
            getTotalHPburn();
        }
    }

    #endregion

    #region 技能設定

    interface ISkillNovice
    {
        int 普通攻擊();
    }

    interface ISkillSwordsman
    {
        int 狂擊();
        void 快速回復();
    }

    interface ISkillKnight
    {
        int 怪物互擊();
        void 靈氣劍();
    }

    interface ISkillLordKnight
    {
        int 集中攻擊();
        void 生命點燃();
    }

    #endregion

    #endregion


    #region 怪物設定

    public abstract class Monster
    {
        public string Name { get; private set; }
        public short EXP { get; private set; }
        public short HIT { get; private set; }
        public short HP { get; private set; }
        //public TimeSpan RebornTime { get; private set; }
    }


    public class POLI : Monster
    {
        public string img { get; private set; }
    }

    #endregion


    #region NPC設定

    #endregion





}
