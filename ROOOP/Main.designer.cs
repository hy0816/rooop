﻿namespace ROOOP
{
    public partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.pb_HP = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gb_Name = new System.Windows.Forms.GroupBox();
            this.lb_SP = new System.Windows.Forms.Label();
            this.lb_EXP = new System.Windows.Forms.Label();
            this.lb_HP = new System.Windows.Forms.Label();
            this.lb_Step = new System.Windows.Forms.Label();
            this.lb_LV = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pb_SP = new System.Windows.Forms.ProgressBar();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.pb_EXP = new System.Windows.Forms.ProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_Point = new System.Windows.Forms.Label();
            this.ll_INT = new System.Windows.Forms.LinkLabel();
            this.ll_VIT = new System.Windows.Forms.LinkLabel();
            this.ll_STR = new System.Windows.Forms.LinkLabel();
            this.label15 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lb_ATK = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lb_STR = new System.Windows.Forms.Label();
            this.lb_VIT = new System.Windows.Forms.Label();
            this.lb_INT = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_Cmd = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gb_Name.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(47, 43);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 33);
            this.button1.TabIndex = 1;
            this.button1.Text = "新增角色";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(487, 12);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(101, 108);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabControl1.Location = new System.Drawing.Point(8, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(584, 378);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(576, 349);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "角色";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(576, 349);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "戰鬥";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(14, 44);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 16);
            this.label16.TabIndex = 20;
            this.label16.Text = "HP";
            // 
            // pb_HP
            // 
            this.pb_HP.Location = new System.Drawing.Point(45, 48);
            this.pb_HP.Name = "pb_HP";
            this.pb_HP.Size = new System.Drawing.Size(200, 8);
            this.pb_HP.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.gb_Name);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(592, 129);
            this.panel1.TabIndex = 6;
            // 
            // gb_Name
            // 
            this.gb_Name.Controls.Add(this.lb_SP);
            this.gb_Name.Controls.Add(this.lb_EXP);
            this.gb_Name.Controls.Add(this.lb_HP);
            this.gb_Name.Controls.Add(this.lb_Step);
            this.gb_Name.Controls.Add(this.lb_LV);
            this.gb_Name.Controls.Add(this.label16);
            this.gb_Name.Controls.Add(this.pb_HP);
            this.gb_Name.Controls.Add(this.label19);
            this.gb_Name.Controls.Add(this.pb_SP);
            this.gb_Name.Controls.Add(this.label18);
            this.gb_Name.Controls.Add(this.label17);
            this.gb_Name.Controls.Add(this.pb_EXP);
            this.gb_Name.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.gb_Name.Location = new System.Drawing.Point(8, 12);
            this.gb_Name.Name = "gb_Name";
            this.gb_Name.Size = new System.Drawing.Size(348, 108);
            this.gb_Name.TabIndex = 27;
            this.gb_Name.TabStop = false;
            this.gb_Name.Text = "name";
            // 
            // lb_SP
            // 
            this.lb_SP.AutoSize = true;
            this.lb_SP.BackColor = System.Drawing.Color.Transparent;
            this.lb_SP.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_SP.Location = new System.Drawing.Point(254, 62);
            this.lb_SP.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_SP.Name = "lb_SP";
            this.lb_SP.Size = new System.Drawing.Size(28, 14);
            this.lb_SP.TabIndex = 29;
            this.lb_SP.Text = "0/0";
            this.lb_SP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lb_EXP
            // 
            this.lb_EXP.AutoSize = true;
            this.lb_EXP.BackColor = System.Drawing.Color.Transparent;
            this.lb_EXP.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_EXP.Location = new System.Drawing.Point(254, 81);
            this.lb_EXP.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_EXP.Name = "lb_EXP";
            this.lb_EXP.Size = new System.Drawing.Size(28, 14);
            this.lb_EXP.TabIndex = 28;
            this.lb_EXP.Text = "0/0";
            this.lb_EXP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lb_HP
            // 
            this.lb_HP.AutoSize = true;
            this.lb_HP.BackColor = System.Drawing.Color.Transparent;
            this.lb_HP.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_HP.Location = new System.Drawing.Point(254, 44);
            this.lb_HP.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_HP.Name = "lb_HP";
            this.lb_HP.Size = new System.Drawing.Size(28, 14);
            this.lb_HP.TabIndex = 27;
            this.lb_HP.Text = "0/0";
            this.lb_HP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lb_Step
            // 
            this.lb_Step.BackColor = System.Drawing.Color.Transparent;
            this.lb_Step.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_Step.Location = new System.Drawing.Point(170, 23);
            this.lb_Step.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_Step.Name = "lb_Step";
            this.lb_Step.Size = new System.Drawing.Size(75, 16);
            this.lb_Step.TabIndex = 26;
            this.lb_Step.Text = "Novice";
            this.lb_Step.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lb_LV
            // 
            this.lb_LV.AutoSize = true;
            this.lb_LV.BackColor = System.Drawing.Color.Transparent;
            this.lb_LV.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_LV.Location = new System.Drawing.Point(42, 23);
            this.lb_LV.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_LV.Name = "lb_LV";
            this.lb_LV.Size = new System.Drawing.Size(15, 16);
            this.lb_LV.TabIndex = 25;
            this.lb_LV.Text = "0";
            this.lb_LV.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(14, 23);
            this.label19.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(22, 16);
            this.label19.TabIndex = 20;
            this.label19.Text = "LV";
            // 
            // pb_SP
            // 
            this.pb_SP.Location = new System.Drawing.Point(45, 66);
            this.pb_SP.Name = "pb_SP";
            this.pb_SP.Size = new System.Drawing.Size(200, 8);
            this.pb_SP.TabIndex = 21;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(11, 80);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(30, 16);
            this.label18.TabIndex = 24;
            this.label18.Text = "EXP";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(15, 62);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 16);
            this.label17.TabIndex = 22;
            this.label17.Text = "SP";
            // 
            // pb_EXP
            // 
            this.pb_EXP.Location = new System.Drawing.Point(45, 84);
            this.pb_EXP.Name = "pb_EXP";
            this.pb_EXP.Size = new System.Drawing.Size(200, 8);
            this.pb_EXP.TabIndex = 23;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lb_Point);
            this.groupBox1.Controls.Add(this.ll_INT);
            this.groupBox1.Controls.Add(this.ll_VIT);
            this.groupBox1.Controls.Add(this.ll_STR);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.lb_ATK);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lb_STR);
            this.groupBox1.Controls.Add(this.lb_VIT);
            this.groupBox1.Controls.Add(this.lb_INT);
            this.groupBox1.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox1.Location = new System.Drawing.Point(362, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(119, 108);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "角色素質";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lb_Point
            // 
            this.lb_Point.AutoSize = true;
            this.lb_Point.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lb_Point.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_Point.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_Point.Location = new System.Drawing.Point(95, 0);
            this.lb_Point.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_Point.Name = "lb_Point";
            this.lb_Point.Size = new System.Drawing.Size(24, 18);
            this.lb_Point.TabIndex = 35;
            this.lb_Point.Text = "10";
            // 
            // ll_INT
            // 
            this.ll_INT.AutoSize = true;
            this.ll_INT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll_INT.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ll_INT.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.ll_INT.LinkColor = System.Drawing.Color.RoyalBlue;
            this.ll_INT.Location = new System.Drawing.Point(89, 84);
            this.ll_INT.Name = "ll_INT";
            this.ll_INT.Size = new System.Drawing.Size(16, 16);
            this.ll_INT.TabIndex = 34;
            this.ll_INT.TabStop = true;
            this.ll_INT.Text = ">";
            this.ll_INT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ll_INT_LinkClicked);
            // 
            // ll_VIT
            // 
            this.ll_VIT.AutoSize = true;
            this.ll_VIT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll_VIT.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ll_VIT.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.ll_VIT.LinkColor = System.Drawing.Color.RoyalBlue;
            this.ll_VIT.Location = new System.Drawing.Point(89, 63);
            this.ll_VIT.Name = "ll_VIT";
            this.ll_VIT.Size = new System.Drawing.Size(16, 16);
            this.ll_VIT.TabIndex = 32;
            this.ll_VIT.TabStop = true;
            this.ll_VIT.Text = ">";
            this.ll_VIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ll_VIT_LinkClicked);
            // 
            // ll_STR
            // 
            this.ll_STR.AutoSize = true;
            this.ll_STR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ll_STR.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ll_STR.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.ll_STR.LinkColor = System.Drawing.Color.RoyalBlue;
            this.ll_STR.Location = new System.Drawing.Point(89, 42);
            this.ll_STR.Name = "ll_STR";
            this.ll_STR.Size = new System.Drawing.Size(16, 16);
            this.ll_STR.TabIndex = 30;
            this.ll_STR.TabStop = true;
            this.ll_STR.Text = ">";
            this.ll_STR.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ll_STR_LinkClicked);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(9, 21);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 16);
            this.label15.TabIndex = 18;
            this.label15.Text = "ATK";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(9, 42);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "STR";
            // 
            // lb_ATK
            // 
            this.lb_ATK.BackColor = System.Drawing.Color.Transparent;
            this.lb_ATK.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_ATK.Location = new System.Drawing.Point(51, 21);
            this.lb_ATK.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_ATK.Name = "lb_ATK";
            this.lb_ATK.Size = new System.Drawing.Size(32, 16);
            this.lb_ATK.TabIndex = 19;
            this.lb_ATK.Text = "0";
            this.lb_ATK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(10, 84);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "INT";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(11, 63);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "VIT";
            // 
            // lb_STR
            // 
            this.lb_STR.BackColor = System.Drawing.Color.Transparent;
            this.lb_STR.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_STR.Location = new System.Drawing.Point(51, 42);
            this.lb_STR.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_STR.Name = "lb_STR";
            this.lb_STR.Size = new System.Drawing.Size(32, 16);
            this.lb_STR.TabIndex = 12;
            this.lb_STR.Text = "0";
            this.lb_STR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lb_VIT
            // 
            this.lb_VIT.BackColor = System.Drawing.Color.Transparent;
            this.lb_VIT.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_VIT.Location = new System.Drawing.Point(51, 63);
            this.lb_VIT.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_VIT.Name = "lb_VIT";
            this.lb_VIT.Size = new System.Drawing.Size(32, 16);
            this.lb_VIT.TabIndex = 16;
            this.lb_VIT.Text = "0";
            this.lb_VIT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lb_INT
            // 
            this.lb_INT.BackColor = System.Drawing.Color.Transparent;
            this.lb_INT.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lb_INT.Location = new System.Drawing.Point(51, 84);
            this.lb_INT.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.lb_INT.Name = "lb_INT";
            this.lb_INT.Size = new System.Drawing.Size(32, 16);
            this.lb_INT.TabIndex = 15;
            this.lb_INT.Text = "0";
            this.lb_INT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 129);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(8, 0, 0, 5);
            this.panel2.Size = new System.Drawing.Size(592, 383);
            this.panel2.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(592, 0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(8, 8, 8, 5);
            this.panel3.Size = new System.Drawing.Size(340, 512);
            this.panel3.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txt_Cmd);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox3.Location = new System.Drawing.Point(8, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox3.Size = new System.Drawing.Size(324, 499);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "執行指令";
            // 
            // txt_Cmd
            // 
            this.txt_Cmd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.txt_Cmd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Cmd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_Cmd.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Cmd.ForeColor = System.Drawing.Color.White;
            this.txt_Cmd.Location = new System.Drawing.Point(5, 21);
            this.txt_Cmd.Multiline = true;
            this.txt_Cmd.Name = "txt_Cmd";
            this.txt_Cmd.ReadOnly = true;
            this.txt_Cmd.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_Cmd.Size = new System.Drawing.Size(314, 473);
            this.txt_Cmd.TabIndex = 0;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 512);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmMain";
            this.Text = "RO物件傳說 守護永恆的OOP";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_Closing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_Closed);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.gb_Name.ResumeLayout(false);
            this.gb_Name.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lb_VIT;
        private System.Windows.Forms.Label lb_INT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ProgressBar pb_HP;
        private System.Windows.Forms.Label lb_ATK;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lb_STR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ProgressBar pb_SP;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ProgressBar pb_EXP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gb_Name;
        private System.Windows.Forms.Label lb_LV;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lb_Step;
        private System.Windows.Forms.Label lb_SP;
        private System.Windows.Forms.Label lb_EXP;
        private System.Windows.Forms.Label lb_HP;
        private System.Windows.Forms.LinkLabel ll_INT;
        private System.Windows.Forms.LinkLabel ll_VIT;
        private System.Windows.Forms.LinkLabel ll_STR;
        private System.Windows.Forms.Label lb_Point;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.TextBox txt_Cmd;
    }
}