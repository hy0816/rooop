﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ROOOP
{
    public partial class Login : Form
    {
        public Tool tool = new Tool();
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            BGM();
            tool.SetLog( new string[] {
                "BGM();", "音樂響起~"
            }); 
        }


        System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
        private void BGM()
        {
            //來點RO的登入背景音樂比較有FU
            sp.SoundLocation = Application.StartupPath + "\\data\\login.wav";
            sp.Play();
        }
        
        private void btn_LOGIN_Click(object sender, EventArgs e)
        {
            string id = txt_ID.Text;
            string psw = txt_PSW.Text;
            //驗證帳號密碼
            if (id == "ROOOP" && psw == "ROOOP") {
                //登入成功，停止BGM，關閉LoginForm，開啟MainForm。
                sp.Stop();
                Program.fLogin.Close();
                Program.fNewRole.Show();
                //Program.fMain.Show();
            }
            else
            {
                //登入失敗
                MessageBox.Show("錯誤的帳號/密碼");
            }
        }



    }
}
