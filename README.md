﻿## ROOOP 物件傳說 守護永恆的OOP

開發ROOOP宗旨：

1. 用物件導向為架構來開發一款仿RO的物件導向教學小程式。
2. 秉持著學習者「學的有興趣，才會繼續學」的道理，讓物件導向學習變得有趣。
3. 開發者也可以順道練習。
4. 就這樣吧！等我想到再慢慢補上...


---

## 怎麼下載？

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---